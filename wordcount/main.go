//go:build !solution

package main

import (
	"fmt"
	"os"
	"strings"
)

func main() {
	m := make(map[string]int)
	for _, file := range os.Args[1:] {
		b, err := os.ReadFile(file)
		if err != nil {
			panic(err)
		}
		s := fmt.Sprintf("%s", b)
		for _, x := range strings.Split(s, "\n") {
			m[x]++
		}
	}
	for k, v := range m {
		if v >= 2 {
			fmt.Printf("%d\t%s\n", v, k)
		}
	}
}
