//go:build !solution

package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
)


func main() {
	for _, url := range os.Args[1:] {
		r, err := http.Get(url)
		if err != nil {
			fmt.Fprintf(os.Stderr, "fetch: Get %s: %v\n", url, err)
			os.Exit(1)
		}
		defer r.Body.Close()
		body, err := io.ReadAll(r.Body)
		if err != nil {
			fmt.Fprintf(os.Stderr, "fetch: Get %s: %v\n", url, err)
			os.Exit(1)
		}
		fmt.Printf("%s", body)
	}
}
